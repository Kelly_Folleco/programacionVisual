<?php

//El arreglo es un conjunto de datos utilizados como una sola variables, contenedor de datos

//$arrego = array (elementos);
//$arreglo= [];     
                //     0        1        2
$zapatillas = array ('Nike','Reebok','Jordan');

//Funciones que sirven para imprimir en pantalla echo print, print_r, var_dump
//echo.-no imprime el detalle del arrego, solo cadena de caracteres
//print_r.-imprime cada unos de los elementos del arreglos
//var_dump.-imprime cada unos de los elementos con su detalle respectivo

echo "<h1>Arreglo con var_dump</h1>";
var_dump($zapatillas);
echo "<br>";
echo"<h1> Elemento del Arreglo con echo </h1>";
echo $zapatillas[1]."<br>";
echo "<br>";

echo "<h1>Despliegue de los elementos del Arreglo con foreach</h1>";
foreach ($zapatillas as $zapatilla) {
    echo $zapatilla."<br";
}

echo "<br>";
echo "<h1>Arreglos con identificaciones</h1>";

$personas=[array('nombre'=>'Roger Bueno',
    'direccion'=>'Los pericos',
    'sexo' =>'Masculino',
    'telefono' =>'099999999',
    'edad' =>18),
    array('nombre'=>'Cristopher Paredes',
    'direccion'=>'Los pericos',
    'sexo' =>'Masculino',
    'telefono' =>'099999999',
    'edad' =>18),
    array('nombre'=>'Jostin Vera',
    'direccion'=>'Los pericos',
    'sexo' =>'Masculino',
    'telefono' =>'099999999',
    'edad' =>18),
];

var_dump($personas);
echo "<br>";
print_r($personas);
echo "<br>";
echo "<br>";
echo "El nombre es: ".$personas['nombre']." la edad es: ".$personas['edad'];
echo "<br>";
echo "<br>";
foreach ($personas as $key => $persona){
    echo "La persona ".$persona['nombre']." tiene la direccion: ".$persona['direccion'].", su sexo es: "
            .$persona['sexo']. ", su telefono es: ".$persona['telefono']." y su edad es de: ".$persona['edad'];
    echo "<br>";
}