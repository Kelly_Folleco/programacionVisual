<?php

/* 
 Un bucle o ciclo, en programación, es una secuencia de instrucciones de código que se ejecuta repetidas veces,
 * hasta que la condicion asignada a dicho bucle deja de cumplirse.
 * Los tres bucles más utilizados en programación son el bucle while, el bucle for y el bucle do-while.
 * while(condición){
        * Secuencia de instrucciones dadas;
  
  }
 *
 *for ($i=0;$i<=10;$i++){
 *          acción; 
 *}
 * 
 * INCREMENTALES-DECREMENTALES
 * $num ++; $num-$num +1; $num+1;
 * $num --; $num-$num -1; $num-1;
 * 
 * /*
 * do{
 *      Secuencia de instrucciones dadas;
 * }while(condición)
 */
$num =['num'];
if(isset($_GET['num'])){
    $in=0;
    echo "<hl>Tabla de multiplicar del ".$num." dada por while </hl>";
    while($in<=10){
        echo $num. " * " .$in. " = ".$num*$in." <br>";
        $in++;
    }
    echo "<br>";
    echo "<hl>Tabla de multiplicar del ".$num." dada por FOR </hl>";
        
    for($i=0;$i<=10;$i++){
        echo $num. " * " .$i. " = ".$num*$i."<br>";
    }
    $cre=0;
    echo "<br>";
    echo "<hl>Tabla de multiplicar del ".$num."dada por DO-WHILE </hl>";
    do{
        echo $num. " * " .$cre. " = ".$num*$cre. " <br>";
        $cre++;
    }while($cre <= 10);
}else{
    echo "No existe ninguna variable enviada por GET....!!!";
}
    
