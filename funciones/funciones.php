<?php

//Una funcion es un conjunto de instrucciones agrupadas bajo un nombre concreto
//y que puede reutilizarse solamente cuadno invocan a la funcion, tantas veces como
//queramos

//function(nombre de la funcion(<parametros>)){
//      conjunto de instrucciones;
//}

function nombre(){
    echo "Mi nombre es Kelly Nathalia";
}
function apellidos(){
    echo "Mis apellidos son Folleco Yanez";
}

//nombre(); //llamar a la funcion

//apellidos();

function nombresCompletos(){
    nombre()." ".apellidos();
}

//nombresCompletos();
//funcion
function listaEstudiantes(){
    $html = "Cristopher Paredes<br>";
    $html.= "Jostin Vera<br>";
    $html.= "Katya Valarezo<br>";
    $html.= "Roger Bueno<br>";
    $html.= "Yomaira Espinoza<br>";
    
    $cadena = "Mi nombre es Kelly ";
    $cadena .= "Mi apellidos es Folleco";
    return $html;
}
//Llamado de la funcion;
//echo listaEstudiantes();


//Ejemplo

function tabla ($numero){
    $tabla = "<h1> La tabla de multiplicar del ".$numero."</h1><br>";
    for($i=0;$i<=10;$i++){
        $tabla.="El resultado de la multiplicación de ".$numero. " * " .$i. " = ".$numero*$i."<br>";
    }
    return $tabla;
}
echo tabla(5);
