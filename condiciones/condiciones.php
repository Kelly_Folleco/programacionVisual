<?php


// if else
// El constructor if es una de las caracteristicas mas importantes de muchos lenguajes, incluido PHP.Permite la ejecucion
// de fragmentos de codigo.PHP dispone de una estructura if que es similar a la de C:

//if (condicion){
// codigo a ejecutarse si la condicion es correcta
//}else{
 
// se ejecuta si la condicion es incorrecta
//}

//Ejemplo:

$edad = 18 ;
if ($edad < 18){
    //echo, print, print_r, var_dump
    echo "La persona es menor de edad y no puede votar";
}else{
   echo "La persona es mayor de edad";
}

//else if

// Como su nombre lo sugiere, es una combinacion de if y else. Del mismo modo que else,
//extiende una sentencia if para ejecutar una sentencia diferente en caso que la expresion if
//original se evalúe como false. Sin embargo, a diferencia de else, esa expresion alternativa
//
echo "<br>";
echo "<hl>elseif</hl>";
$a= $_GET['a'];//5
$b= $_GET['b'];//8

if ($a>b){
    echo "a es mayor que b";
}elseif ($a == $b){
    echo "a es igual que b";
}else{
    echo "a es menor que b";
}

echo "<br>";
echo "<hl>elseif</hl>";

$color = $_GET['color'];
//isset.- Si existe
//empty.- Si esta vacío
//is_null.- Si es null
if(isset($color)){
    switch ($color){
        case 0;
            echo "El color es igual a Rojo";
            break;
        case 1:
            echo "El color es igual a Verde";
            break;
        case 2:
            echo "El color es igual a Azul";
            break;
        case 3:
            echo "El color es igual a Amarillo";
            break;    
    } 
}