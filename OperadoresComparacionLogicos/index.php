<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//OPERADORES DE COMPARACIÓN
//
// == compara si el valor numero o cadena de caracter es igual
// === compara lo del primer comparador y tambien el typo de variable
//!= compara que sean diferentes
//!== compara que sean diferente y diferente tipo de dato
//<= menor igual a un valor dado
//>= mayor igual a un valor dado


//OPERADORES LÓGICOS
//
// && and : Este debe ser verdadero y verdadero  para que se ha igual a verdader v && v = v
// || or

//EJEMPLO
// v && v = v
// v && f = f
// f && v = f
// f && f = f

if (condicion1 && condicion2 ){
    //verdadero
}else{
    //falso
}

//EJEMPLO
// v && v = v
// v && f = v
// f && v = v
// f && f = f

if (condicion1 || condicion2 ){
    //verdadero
}else{
    //falso
}